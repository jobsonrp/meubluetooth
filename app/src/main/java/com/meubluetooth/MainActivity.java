package com.meubluetooth;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    int ATIVA_BT = 1;

    BluetoothAdapter meuBTAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        meuBTAdapter = BluetoothAdapter.getDefaultAdapter();

        if (meuBTAdapter == null){
            Toast.makeText(getApplicationContext(), "Dispositivo sem Bluetooth.", Toast.LENGTH_LONG).show();
        } else if (!meuBTAdapter.isEnabled()) {
            Intent ativaBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(ativaBT, ATIVA_BT);
        }

    }
}
